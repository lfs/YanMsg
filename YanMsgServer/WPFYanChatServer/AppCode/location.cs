﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace WPFYanChatServer
{
    internal class Location
    {
        /// <summary>
        /// 监控窗体与显示器的左边距
        /// </summary>
        public static Double WindowLeft { get; set; }

        /// <summary>
        /// 监控窗体与显示器顶部边距
        /// </summary>
        public static Double WindowTop { get; set; }





    }


    public class ServerLog
    {
        static void writeLog(string str)
        {
            if (!Directory.Exists("ErrLog"))
            {
                Directory.CreateDirectory("ErrLog");
            }

            using (StreamWriter sw = new StreamWriter(@"ErrLog\ErrLog.txt", true))
            {
                sw.WriteLine(str);
                sw.WriteLine("---------------------------------------------------------");
                sw.Close();
            }
        }
        public ServerLog(string str1, string str2)
        {
            日志类型 = str1;
            时间 = DateTime.Now.ToString();
            内容 = str2;
        }
        public string 日志类型 { get; set; }
        public string 时间 { get; set; }
        public string 内容 { get; set; }
    }

    public class OnlineUserinfo
    {
        public OnlineUserinfo(string str1, string str2)
        {
            用户账号 = str1;
            用户IP = str2;
            时间 = DateTime.Now.ToString();

        }
        public string 用户账号{ get; set; }
        public string 用户IP { get; set; }
        public string 时间 { get; set; }

    }

}