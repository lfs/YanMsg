using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using WPFYanChatServer;
using agsXMPP.protocol;
using agsXMPP.protocol.iq;
using agsXMPP.protocol.iq.auth;
using agsXMPP.protocol.iq.roster;
using agsXMPP.protocol.client;
using agsXMPP.Xml;
using agsXMPP.Xml.Dom;
using agsXMPP;
using System.Linq;
using System.Collections.Generic;

namespace WPFYanChatServer
{
	/// <summary>
	/// Zusammenfassung f黵 XMPPSeverConnection.
	/// </summary>
	public class XmppSeverConnection
	{
		public XmppSeverConnection()
		{		
			streamParser = new StreamParser();
			streamParser.OnStreamStart		+= new StreamHandler(streamParser_OnStreamStart);
			streamParser.OnStreamEnd		+= new StreamHandler(streamParser_OnStreamEnd);
			streamParser.OnStreamElement	+= new StreamHandler(streamParser_OnStreamElement);	
           
		}

		public XmppSeverConnection(Socket sock) : this()
        {
            try
            {
                m_Sock = sock;
                m_Sock.BeginReceive(buffer, 0, BUFFERSIZE, 0, new AsyncCallback(ReadCallback), null);
            }
            catch (Exception ex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "XmppSeverConnection" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
		}

        #region(变量)
        private StreamParser streamParser;
        private Socket m_Sock;
        private const int BUFFERSIZE = 1024;
        private byte[] buffer = new byte[BUFFERSIZE];
        public Jid jid;
        public delegate void mydelegate(string str);
        #endregion
	
		public void ReadCallback(IAsyncResult ar) 
		{
            try
            {
                int bytesRead = m_Sock.EndReceive(ar);

                if (bytesRead > 0 || m_Sock != null)
                {
                    streamParser.Push(buffer, 0, bytesRead);

                    // Not all data received. Get more.
                    if (m_Sock!=null)
                    {
                        m_Sock.BeginReceive(buffer, 0, BUFFERSIZE, 0, new AsyncCallback(ReadCallback), null);
                    }
                }
                else
                {
                    m_Sock.Shutdown(SocketShutdown.Both);
                    m_Sock.Close();
                }
            }
            catch (SocketException ex)
            {

                if (m_Sock != null)
                {
                    ServerData.onlineuser.RemoveAll(d => d.m_Sock == m_Sock);
                    Presence pres = new Presence();
                    foreach (XmppSeverConnection con in ServerData.onlineuser)
                    {
                        pres.From = this.jid;
                        if (this.jid != null && con.jid.User != this.jid.User)
                        {
                            pres.To = con.jid;
                            pres.Type = PresenceType.unavailable;
                            con.Send(pres);
                        }
                    }
                    m_Sock.Shutdown(SocketShutdown.Both);
                    m_Sock.Close();
                }
            }
            catch (IOException ioex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "ReadCallback" + ioex.Message.ToString(), UserDateTime = DateTime.Now });

            }
            catch (SystemException sysex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "ReadCallback" + sysex.Message.ToString(), UserDateTime = DateTime.Now });

            }
		}

		private void Send(string data) 
		{
            try
            {
                // Convert the string data to byte data using ASCII encoding.
                byte[] byteData = Encoding.UTF8.GetBytes(data);

                // Begin sending the data to the remote device.
                m_Sock.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), null);
            }
            catch (Exception ex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "XmppSeverConnection" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
		}

		private void SendCallback(IAsyncResult ar) 
		{
			try 
			{
				// Complete sending the data to the remote device.
				int bytesSent = m_Sock.EndSend(ar);

			} 
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
			}
		}
	
		
		public void Stop()
		{
            try
            {
                Send("</stream:stream>");
                //			client.Close();
                //			_TcpServer.Stop();

                m_Sock.Shutdown(SocketShutdown.Both);
                m_Sock.Close();
            }
            catch (Exception ex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "Stop" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
		}
			
		
		#region << Properties and Member Variables >>
		private string		m_SessionId		= null;

		public string SessionId
		{
			get
			{
				return m_SessionId;
			}
			set
			{
				m_SessionId = value;
			}
		}
		#endregion

		private void streamParser_OnStreamStart(object sender, Node e)
		{
            try
            {
                SendOpenStream();
            }
            catch (Exception ex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "streamParser_OnStreamStart" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
		}

		private void streamParser_OnStreamEnd(object sender, Node e)
		{
            try
            {
                ServerData.onlineuser.RemoveAll(d => d.m_Sock == m_Sock);
                Presence pres = new Presence();
                foreach (XmppSeverConnection con in ServerData.onlineuser)
                {
                    pres.From = this.jid;
                    if (con.jid.User != this.jid.User)
                    {
                        pres.To = con.jid;
                        pres.Type = PresenceType.unavailable;
                        con.Send(pres);
                    }
                }
                m_Sock.Shutdown(SocketShutdown.Both);
                m_Sock.Close();
                m_Sock = null;
            }
            catch (Exception ex)
            {

                new Yan_LogB().Insert(new Yan_Log { Memo = "streamParser_OnStreamEnd" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
           

		}

        /// <summary>
        /// 三个函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void streamParser_OnStreamElement(object sender, Node e)
        {
            try
            {
                if (e.GetType() == typeof(Presence))
                {
                    Presence pres = e as Presence;
                    //处理用户上线消息
                    if (pres.Show == ShowType.chat && pres.Type == PresenceType.available)
                    {
                        pres.From = this.jid;
                        foreach (XmppSeverConnection con in ServerData.onlineuser)
                        {
                            if (con.jid.User != this.jid.User)
                            {
                                pres.Value = m_Sock.RemoteEndPoint.ToString().Split(':')[0];
                                pres.To = con.jid;
                                con.Send(pres);
                            }
                        }
                    }
                    //处理好友离线消息
                    else if (pres.Type == PresenceType.unavailable)
                    {
                        pres.From = this.jid;

                        ServerData.onlineuser.Remove(this);


                        foreach (XmppSeverConnection con in ServerData.onlineuser)
                        {
                            if (con.jid.User != this.jid.User)
                            {
                                pres.To = con.jid;
                                con.Send(pres);
                            }
                        }

                    }

                }
                else if (e.GetType() == typeof(agsXMPP.protocol.client.Message))
                {
                    agsXMPP.protocol.client.Message msg = e as agsXMPP.protocol.client.Message;
                    //点对点聊
                    if (msg.Type == MessageType.chat)
                    {
                        foreach (XmppSeverConnection con in ServerData.onlineuser)
                        {
                            if (con.jid.User == msg.To.User)
                            {
                                msg.From = jid;
                                con.Send(msg);
                                new YanChat_MsgB().Insert(new YanChat_Msg { MsgContent = msg.Body, UserForm = msg.From.User, UserTo = msg.To.User, CreateDate = DateTime.Now });
                            }
                        }
                    }//群聊
                    else if (msg.Type == MessageType.groupchat)
                    {
                        List<string> ListGUser = new YanChatHelp().GetGroupUsers(msg.To.User);
                        foreach (XmppSeverConnection con in ServerData.onlineuser)
                        {
                            if (ListGUser.Contains(con.jid.User))
                            {
                                con.Send(msg);
                            }
                        }
                    }
                    else if (msg.Type == MessageType.headline)//接受命令
                    {
                        if (msg.Body == "UpdateGroup")
                        {
                            foreach (XmppSeverConnection con in ServerData.onlineuser)
                            {
                                if (con.jid.User != msg.From.User)
                                {
                                    msg.Type = MessageType.headline;
                                    msg.Body = "UpdateGroup";
                                    con.Send(msg.ToString());
                                }
                            }
                        }
                    }
                }
                else if (e.GetType() == typeof(IQ))
                {
                    ProcessIQ(e as IQ);
                }
            }
            catch (Exception ex)
            {

                new Yan_LogB().Insert(new Yan_Log { Memo = "streamParser_OnStreamElement" + ex.Message.ToString(), UserDateTime=DateTime.Now });
            }
        

        }

        private void ProcessIQ(IQ iq)
        {
            try
            {
                //用户认证
                if (iq.Query.GetType() == typeof(Auth))
                {
                    Auth auth = iq.Query as Auth;
                    switch (iq.Type)
                    {
                        case IqType.get:
                            iq.SwitchDirection();
                            iq.Type = IqType.result;
                            auth.AddChild(new Element("password"));
                            Send(iq);
                            break;
                        case IqType.set:
                            // 进行登录认证
                            //   if (AccountBus.CheckLogin(auth.Username, auth.Password, this.SessionId))

                            if (YanChatHelp.CheckLogin(auth.Username, auth.Password))
                            {
                                this.jid = new Jid(auth.Username, Main.ServerIP, auth.Username);

                                if (ServerData.onlineuser.Where(d => d.jid.User == auth.Username).Count() > 0)
                                {
                                    ServerData.onlineuser.RemoveAll(d => d.jid.User == auth.Username);
                                }
                                ServerData.onlineuser.Add(this);

                                iq.SwitchDirection();
                                iq.Type = IqType.result;
                                iq.Query = null;
                                Send(iq);
                                new Yan_LogB().Insert(new Yan_Log { Memo = "用户登录成功" + auth.Username, UserDateTime = DateTime.Now });

                            }
                            else
                            {
                                //登录失败返回错误信息
                                iq.SwitchDirection();
                                iq.Type = IqType.error;
                                iq.Query = null;
                                Send(iq);
                                new Yan_LogB().Insert(new Yan_Log { Memo = "用户登录失败" + auth.Username, UserDateTime = DateTime.Now });

                            }
                            break;
                    }

                }
                else if (iq.Query.GetType() == typeof(Roster))
                {
                    ProcessRosterIQ(iq);

                }
            }
            catch (Exception ex)
            {
                new Yan_LogB().Insert(new Yan_Log { Memo = "ProcessIQ" + ex.Message.ToString(), UserDateTime=DateTime.Now });
            }
         

        }

        // 发送在线用户列表
        private void ProcessRosterIQ(IQ iq)
        {
            try
            {
                if (iq.Type == IqType.get)
                {

                    //获取用户列表
                    foreach (var item in ServerData.onlineuser)
                    {

                        RosterItem ri = new RosterItem();
                        ri.Name = item.jid.User;
                        ri.Subscription = SubscriptionType.both;
                        ri.Jid = new Jid(ri.Name, Main.ServerIP, ri.Name);
                        ri.Value = item.m_Sock.RemoteEndPoint.ToString().Split(':')[0];
                        iq.Query.AddChild(ri);
                        iq.Type = IqType.result;
                    }

                    Send(iq);

                }
            }
            catch (Exception ex)
            {

                new Yan_LogB().Insert(new Yan_Log { Memo = "ProcessRosterIQ" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
           
        }


        private void SendOpenStream()
        {
            try
            {
                // Recv:<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' from='myjabber.net' id='1075705237'>

                // Send the Opening Strem to the client
                string ServerDomain = Main.ServerIP;

                this.SessionId = WPFYanChatServer.SessionId.CreateNewId();


                StringBuilder sb = new StringBuilder();

                sb.Append("<stream:stream from='");
                sb.Append(ServerDomain);

                sb.Append("' xmlns='");
                sb.Append(agsXMPP.Uri.CLIENT);

                sb.Append("' xmlns:stream='");
                sb.Append(agsXMPP.Uri.STREAM);

                sb.Append("' id='");
                sb.Append(this.SessionId);

                sb.Append("'>");

                Send(sb.ToString());
            }
            catch (Exception ex)
            {

                new Yan_LogB().Insert(new Yan_Log { Memo = "SendOpenStream" + ex.Message.ToString(), UserDateTime = DateTime.Now });
            }
        }


		private void Send(Element el)
		{
			Send(el.ToString());
		}



	}
}