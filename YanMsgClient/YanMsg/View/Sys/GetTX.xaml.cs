﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YanMsg.APCode;
using System.Threading;
using FirstFloor.ModernUI.Windows.Controls;
using System.Threading.Tasks;

namespace YanMsg.View
{
    /// <summary>
    /// GetTX.xaml 的交互逻辑
    /// </summary>
    public partial class GetTX : ModernWindow
    {
        public GetTX()
        {
            InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];

            for (int i = 1; i < 29; i++)
            {
                Image imgtx = new Image();
                imgtx.Name = "TX" + i;
                BitmapImage Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + imgtx.Name + ".jpg"));
                imgtx.Width = 65;
                imgtx.Height = 65;
                imgtx.Source = Source;
                Border imgbor= new Border();
                imgbor.BorderThickness=new Thickness(2);
                imgbor.Margin= new Thickness(10,5,0,0);
                imgbor.Child=imgtx;
                imgtx.MouseLeftButtonDown += delegate(Object sender, MouseButtonEventArgs e)
                {
                    Image TX = sender as Image;
                    UserData.ListPeople.SingleOrDefault(d => d.UserName == UserData.UserNumber).tx = TX.Name;
                    MainPeopleList.ModifyPeopleTree();
                    Task UpdateTX = new Task(UpdateTask, TX.Name);
                    UpdateTX.Start();
                };
                txCon.Children.Add(imgbor);
            }
        }


        private void UpdateTask(object txname)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                new YanChatHelp().UpdateUserTX(UserData.UserNumber, txname.ToString());

            }));
        }
       
        private void btConfirim_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
