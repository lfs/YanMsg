﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YanMsg.APCode;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Threading;
using FirstFloor.ModernUI.Windows.Controls;

namespace YanMsg.View
{
    /// <summary>
    /// AddMsg.xaml 的交互逻辑
    /// </summary>
    public partial class AddDayLog : ModernWindow
    {



     


        public AddDayLog()
        {
            this.InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];

            // 在此点之下插入创建对象所需的代码。
            conlogDate.Text = DateTime.Today.ToLongDateString();
        }
        private void conCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void conConfirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                conConfirm.Content = "正在完成...";
                conConfirm.IsEnabled = false;
                Action action = new Action(delegate()
                {
                    Thread.Sleep(1000);
                    Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        InsertGG();
                        this.conConfirm.Content = "确定";
                        conConfirm.IsEnabled = true;

                        MsgManger.Show("填写完成");
                    }), null);
                });
                action.BeginInvoke(null, null);

            }
            catch (Exception error)
            {
                MsgManger.Show(error.Message.ToString());
            }
        }
        private void InsertGG()
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                new DayLog().Insert(new Yan_DayLog() { CrName = UserData.UserNumber, LogUser = UserData.UserNumber, DayLogTitle = conLogindex.Text, DayLog = ChatManger.toRtf(this.conMsgContent), LogDate = DateTime.Parse(conlogDate.Text) });

            }));
        }




    }
}