﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using YanMsg.APCode;

namespace YanMsg.View
{
    /// <summary>
    /// MsgBox.xaml 的交互逻辑
    /// </summary>
    public partial class AddMsg : ModernWindow
    {
        public ObservableCollection<YanChat_File> AddFiles = new ObservableCollection<YanChat_File>();

        public AddMsg()
        {
            InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];
            RootItem.ItemsSource = MainPeopleList.PeopleTree;
            ConFileList.ItemsSource = AddFiles;

        }

        private void ImageFJ_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    string str = ofd.FileName;
                    FileInfo File = new FileInfo(ofd.FileName.ToString());
                    if (!AddFiles.Select(d => d.FileName).Contains(File.Name))
                    {
                        FileStream stream = File.OpenRead();
                        Byte[] buffer = new Byte[stream.Length];
                        //从流中读取字节块并将该数据写入给定缓冲区buffer中
                        stream.Read(buffer, 0, Convert.ToInt32(stream.Length));
                        YanChat_File NewFile = new YanChat_File();
                        NewFile.FileName = File.Name;
                        NewFile.FileContent = buffer;
                        AddFiles.Add(NewFile);
                    }
                    else
                    {
                        MsgManger.Show("已经添加过该附件");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImgDel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image delIMG = sender as Image;
            string strFileName = delIMG.Tag.ToString();
            AddFiles.Remove(AddFiles.SingleOrDefault(d => d.FileName == strFileName));
        }
        private void btSend_Click(object sender, RoutedEventArgs e)
        {
            if (MainPeopleList.PeopleTree.Where(d => d.TreeUserList.Where(m => m.isChecked == true).Count() > 0).Count()>0)
            {
                btSend.IsEnabled = false;
                btSend.Content = "正在发送";
                Task task = new Task(SendMsg);
                task.Start();
            }
          
        }

        public void SendMsg()
        {
            Dispatcher.BeginInvoke(new Action(delegate()
          {
              string strMsgUsrs = "";

              //插入消息数据
              YanChat_MsgContent NewMsgContent = new YanChat_MsgContent();
              NewMsgContent.MSGType = (conMSGType.SelectedItem as ComboBoxItem).Tag.ToString();
              NewMsgContent.MSGContent = ChatManger.toRtf(conMsgContent);
              NewMsgContent.MSGTitle = conMsgTitle.Text;
              NewMsgContent.MSGCRDate = DateTime.Now;
              NewMsgContent.MSGCRPeople = UserData.UserNumber;
              NewMsgContent.MSGISDele = "N";
              NewMsgContent.MsgISShow = "N";

              new YanMsg.APCode.YanChat_MsgContentB().Insert(NewMsgContent);

              //插入用户消息关联数据
              foreach (var item in MainPeopleList.PeopleTree)
              {
                  foreach (TreeUser user in item.TreeUserList)
                  {
                      if (user.isChecked)
                      {
                          strMsgUsrs = strMsgUsrs + user.UserName + ",";
                          new YanChat_UserMsgB().Insert(new YanChat_UserMsg { MsgID = NewMsgContent.id.ToString(), MsgUser = user.UserName, MsgIsRead = "0", Remark = NewMsgContent.MSGTitle, MsgType = "0", Remark1 = AddFiles.Count().ToString(), cruser = UserData.UserNumber, crdate = DateTime.Now });
                      }
                  }
              }
              //添加附件
              foreach (YanChat_File File in AddFiles)
              {
                  File.GGID = NewMsgContent.id.ToString();
                  new YanChat_FileB().Insert(File);

              }
              btSend.Content = "发送";
              btSend.IsEnabled = true;
              StatusMsg.Text = "发送完成";
          }), null);

        }
    }
}
