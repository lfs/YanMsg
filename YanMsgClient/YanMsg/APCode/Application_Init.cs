﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using YanMsg.View;
using Microsoft.Windows.Shell;

namespace YanMsg.APCode
{
    internal class Application_Init
    {
        /// <summary>
        /// 初始化托盘区图标
        /// </summary>
        public static void Initial()
        {

            UserData.notifyIcon.Icon = UserData.Icon_Login;
            UserData.notifyIcon.Visible = true;
            UserData.notifyIcon.MouseDoubleClick += new MouseEventHandler(notifyIcon_MouseDoubleClick);
       
        }

        public static void maininit()
        {
            UserData.menuItems = new MenuItem[2];
            UserData.menuItems[0] = new MenuItem();
            UserData.menuItems[0].Text = "注销";
            UserData.menuItems[0].Click += new EventHandler(loginoffMenu);
            UserData.menuItems[0].DefaultItem = true;
            UserData.menuItems[1] = new MenuItem();
            UserData.menuItems[1].Text = "退出";
            UserData.menuItems[1].Click += new EventHandler(notifyExit_Click);
            UserData.notifyIconMenu = new ContextMenu(UserData.menuItems);
            UserData.notifyIcon.ContextMenu = UserData.notifyIconMenu;
        }

        /// <summary>
        /// 托盘区图标状态
        /// </summary>
        /// <param name="LoginState">登陆状态</param>
        public static void notifyIconUserState(int LoginState)
        {
            try
            {
                switch (LoginState)
                {
                    case 0://在线
                        UserData.notifyIcon.Icon = UserData.Icon_Online;
                        UserData.notifyIcon.Text = "您的当前状态为：在线";
                        break;
                    //case 1://忙碌
                    //    UserData.notifyIcon.Icon = UserData.Icon_Busyness;
                    //    UserData.notifyIcon.Text = "您的当前状态为：忙碌";
                    //    UserData.notifyIcon.Visible = true;
                    //    break;
                    //case 2://请勿打扰
                    //    UserData.notifyIcon.Icon = UserData.Icon_Not;
                    //    UserData.notifyIcon.Text = "您的当前状态为：请勿打扰";
                    //    UserData.notifyIcon.Visible = true;
                    //    break;
                    case 3://离线
                        UserData.notifyIcon.Icon = UserData.Icon_Offline;
                        UserData.notifyIcon.Text = " 您的当前状态为：离线";
                        break;
                    //case 5://隐身
                    //    UserData.notifyIcon.Icon = UserData.Icon_Hide;
                    //    UserData.notifyIcon.Text = "您的当前状态为：隐身";
                    //    UserData.notifyIcon.Visible = true;
                    //    break;
                }
            }
            catch (Exception)
            {
                
            }
        
        }

        /// <summary>
        /// 托盘区图标双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                //如果主窗体已在前台
                if (UserData.MainWindow.Visibility == Visibility.Visible)
                {
                    UserData.MainWindow.Hide();
                }
                else
                {
                    SystemCommands.RestoreWindow(UserData.MainWindow);
                    UserData.MainWindow.Show();
                    UserData.MainWindow.Activate();
                    UserData.MainWindow.Topmost = true;
                }

            }
            catch
            {
            }
        }

        /// <summary>
        /// 单击托盘区“退出”菜单事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void notifyExit_Click(object sender, EventArgs e)
        {
            Exit();
        }


        /// <summary>
        /// 单击托盘区“注销”菜单事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void loginoffMenu(object sender, EventArgs e)
        {
            Login.connection.Close();
            LoginOff();
        }


        public static void LoginOff()
        {

            //UserData.udpReceiveFile.Dispose();
            //UserData.MainWindow.Close();
            //List<string> ListKey = new List<string>();
            //ListKey = UserData.DictionaryChat.Keys.ToList();
            //foreach (string strkey in ListKey)
            //{
            //    UserData.DictionaryChat[strkey].Close();
            //}
            //UserData.LoginWindow.btDengLu.IsEnabled = true;
            //UserData.LoginWindow.Show();
        }
        /// <summary>
        /// 释放托盘区图标
        /// </summary>
        public static void setFreeNotifyIcon()
        {
            UserData.notifyIcon.Visible = false;
            UserData.notifyIcon.Dispose();
        }

        public static void Exit()
        {
            Application_Init.setFreeNotifyIcon();
            Environment.Exit(0);
        }
    }
}