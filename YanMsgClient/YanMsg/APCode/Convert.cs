﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using System.Data;
using System.Windows.Resources;

namespace YanMsg.APCode
{

    public class UserNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            try
            {
                string strUserName = (string)value;
                return UserData.ListPeople.SingleOrDefault(d => d.UserName == strUserName).UserRealName;
            }
            catch (Exception ex)
            {
                MsgManger.Show(ex.Message);
                return "";
            }

        }
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    public class SizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            int intSize = 0;
            if (value != null)
            {
                intSize = (int)value;
            }
            return UpDownLoadFile.ReturnFileSize(intSize);
        }
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
    public class isShowParConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int mixSize = (int)value[0];
                int nowSize = (int)value[1];
                return mixSize == nowSize ? Visibility.Collapsed : Visibility.Visible;
            }
            catch (Exception)
            {
                return Visibility.Visible;
            }
          
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class LineConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strReturn = "";
            try
            {
                string isOnLine = (string)value[0];
                string strUser = (string)value[1];

                if (isOnLine == "Y")
                {
                    strReturn = "(在线)";
                }
                else
                {
                    DataTable dt = new User().GetDTByCommand(" SELECT CC.ShenQingRen, CC.IsSend,CC.remark3,CC.StarTime,CC.remark2, CC.EndTime, CC.LeiBieName, CC.ZhuYaoShiYou FROM GR_CCXJ  CC join Pro_ZiDian ZD on CC.LeiBie=ZD.TypeNO  WHERE  getdate() BETWEEN  CAST(StarTime+ ' '+ (CASE CC.remark2 WHEN '上午' THEN '00:01:00'  ELSE '12:01:00'  END) as datetime) AND CAST(EndTime+ ' '+ (CASE LeiBieName WHEN '上午'  THEN '12:01:00'  ELSE '23:59:00'  END) as datetime)  AND ZD.Class=3 AND CC.IsComPlete='Y'  and CRUser='" + strUser + "'");
                    if (dt.Rows.Count > 0)
                    {
                        strReturn = "(" + dt.Rows[0]["remark3"].ToString() + ")";
                    }
                }
            }
            catch (Exception)
            {
                
            }
            return strReturn;
        }
        public  object[] ConvertBack(object value, Type[] targetType,object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class PhotoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            string isOnLine = (string)value;
            if (isOnLine == "Y")
            {
                BitmapImage Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + "TX3.jpg"));
                return Source;
            }
            else
            {
                FormatConvertedBitmap fcb = new FormatConvertedBitmap(new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + "TX3.jpg")), PixelFormats.Gray32Float, null, 0);
                return fcb;
            }
        }
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }


    public class ImgConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //throw new NotImplementedException();
            //string strSource = "/LuckyLottery;component/person/" + value + ".jpg";
            //return strSource;
            BitmapImage img = new BitmapImage();
            //若要原始文件的站点，可以调用 Application 类的 GetRemoteStream 方法，同时传递标识原始文件的所需站点的 pack URI。 GetRemoteStream 将返回一个 StreamResourceInfo 对象，该对象将原始文件的该站点作为 Stream 公开，并描述其内容类型。
            StreamResourceInfo info = Application.GetRemoteStream(new Uri(value.ToString(), UriKind.Relative));
            img.BeginInit();
            //img.UriSource = new Uri(value.ToString(), UriKind.Relative);
            img.StreamSource = info.Stream;
            img.EndInit();
            return img;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// WPF多值绑定转换器
    /// </summary>
    public class MultiValueConverterSample : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            string isOnLine = values[0].ToString();
            if (isOnLine == "Y")
            {
                BitmapImage Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + values[1].ToString() + ".jpg"));
                return Source;
            }
            else
            {
                FormatConvertedBitmap fcb = new FormatConvertedBitmap(new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + values[1].ToString() + ".jpg")), PixelFormats.Gray32Float, null, 0);
                return fcb;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class MsgConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            string isRead = (string)value;
            return isRead == "0" ? Visibility.Visible : Visibility.Hidden;
        }
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    // 讨论组菜单转化
    public class DelGroupConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string GroupUser = (string)value;
            return GroupUser == UserData.UserNumber;
        }
        public object ConvertBack(object value, Type targetType,object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
    // 讨论组菜单转化
    public class ExitGroupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,System.Globalization.CultureInfo culture)
        {
            string GroupUser = (string)value;
            return !(GroupUser == UserData.UserNumber);
        }
        public object ConvertBack(object value, Type targetType,object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    /// <summary>
    /// 消息中心星号图标绑定
    /// </summary>
    public class isShouchangConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            string isShouchang = (string)value;
            if (isShouchang == "0")
            {
                BitmapImage Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/imgs/" + "Fav_Empty.png"));
                return Source;
            }
            else
            {
                BitmapImage Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/imgs/" + "Fav.png"));
                return Source;
            }
        }
        public object ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    // 是否显示附件按钮
    public class isHasAttchConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string isHasAttch = (string)value;
            if (isHasAttch == "0")
            {
                return Visibility.Hidden;
            }
            else
            {
                return Visibility.Visible;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }


}
