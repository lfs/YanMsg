﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using YanMsg.View;

namespace YanMsg.APCode
{

    internal class MsgManger
    {
        /// <summary>
        /// 弹出普通提示窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void Show(string Message)
        {
            ModernDialog WinCue = new ModernDialog();
            WinCue.Content = Message;
            WinCue.Topmost = true;
            WinCue.Title = "提示";
            WinCue.Show();
        }

        /// <summary>
        /// 弹出普通提示窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void ShowTip(string Message)
        {
            ModernDialog WinCue = new ModernDialog();
            WinCue.Content = Message;
            WinCue.Topmost = true;
            WinCue.Title = "提示";
            WinCue.Show();
        }


        /// <summary>
        /// 弹出普通提示窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void ShowTuiTip(string Message,string strTitle="通知")
        {
            TuiMsg WinCue = new TuiMsg();
            WinCue.Bcode.BBCode = Message;
            WinCue.Left = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Width - WinCue.Width;
            WinCue.Top = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Height;
            WinCue.Topmost = true;
            WinCue.Opacity = 0;
            WinCue.Show();


            //弹出动画
            double TopTo = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Height - WinCue.Height;
            DoubleAnimationUsingKeyFrames p = new DoubleAnimationUsingKeyFrames();
            p.KeyFrames.Add(new LinearDoubleKeyFrame(1, KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
            Storyboard s = new Storyboard();
            DoubleAnimationUsingKeyFrames d = new DoubleAnimationUsingKeyFrames();
            d.KeyFrames.Add(new LinearDoubleKeyFrame(TopTo, KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
            s.Children.Add(d);
            s.Children.Add(p);
            Storyboard.SetTargetProperty(d, new PropertyPath(Canvas.TopProperty));
            Storyboard.SetTargetProperty(p, new PropertyPath(Canvas.OpacityProperty));
            s.Begin(WinCue);





            Task TaskMsgExit = new Task(() =>
            {
                Thread.Sleep(new TimeSpan(0, 0, 30));
                WinCue.Dispatcher.Invoke(new Action(() =>
                {
                    WinCue.Close();
                }));
            });
            TaskMsgExit.Start();
        }

        /// <summary>
        /// 弹出公告窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void ShowGG(string strUser, string strType)
        {
            ModernDialog WinCue = new ModernDialog();
            WinCue.MouseLeftButtonDown += new MouseButtonEventHandler(ShowGG_MouseDoubleClick);
            if (strType == "0")
            {
                if (new YanChat_UserMsgB().GetEntities(d => d.MsgUser == strUser && d.MsgIsRead == "0").Count() > 0)
                {
                    vw_Yan_Chat_MsgUser Chat_MsgUser = new vw_Yan_Chat_MsgUser();
                    Chat_MsgUser = new vw_MsgUser().GetEntities(d => d.MsgIsRead == "0" && d.MsgUser == UserData.UserNumber).OrderByDescending(d=>d.MSGCRDate).First();
                    WinCue.Content = Chat_MsgUser.MSGTitle;
                    WinCue.Title = "公告";
                  //  WinCue.conISRead.Tag = Chat_MsgUser.id;
                    WinCue.Topmost = true;
                    WinCue.Show();
                }
            }
            else
            {
            }
        }

            /// <param name="e"></param>
        private static void ShowGG_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //ViewMsg skins = new ViewMsg();
            //skins.Topmost = true;
            //skins.Show();
            //skins.Activate();

        }


        /// <summary>
        /// 渐变弹出窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void ShowWindows(Window WinCue, string strTitle = "通知")
        {
            WinCue.Opacity = 0;
            WinCue.Show();

            Storyboard s = new Storyboard();
            DoubleAnimationUsingKeyFrames d = new DoubleAnimationUsingKeyFrames();
            d.KeyFrames.Add(new LinearDoubleKeyFrame(1, KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.5))));
            s.Children.Add(d);
            Storyboard.SetTargetProperty(d, new PropertyPath(Canvas.OpacityProperty));
            s.Begin(WinCue);
        }

        /// <summary>
        /// 渐变关闭窗口
        /// </summary>
        /// <param name="Message"></param>
        public static void CloseWindows(Window WinCue)
        {
            WinCue.Close();
            Storyboard s = new Storyboard();
            DoubleAnimationUsingKeyFrames d = new DoubleAnimationUsingKeyFrames();
            d.KeyFrames.Add(new LinearDoubleKeyFrame(0, KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
            s.Children.Add(d);
            Storyboard.SetTargetProperty(d, new PropertyPath(Canvas.OpacityProperty));
            s.Begin(WinCue);
        }
    }

    /// <summary>
    /// An ICommand implementation displaying a message box.
    /// </summary>
    public class ShowXXZXCommand
        : CommandBase
    {
        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        protected override void OnExecute(object parameter)
        {
            MsgManger.ShowWindows(new MsgBox());
        }
    }
}