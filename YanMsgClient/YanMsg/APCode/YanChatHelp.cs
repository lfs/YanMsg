﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace YanMsg.APCode
{

    public class YanChatHelp
    {
        public List<vw_Yan_Chat_user> GetAllUser()
        {
            List<vw_Yan_Chat_user> USERS = new Yan_UserB().GetEntities().ToList();
            return USERS;
        }



        /// <summary>
        /// 获取用户所在讨论组
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public List<YanChat_Group> GetUserGroups(string strUserID)
        {
            var Groups = from o in new UG().GetEntities(d => d.UserID == strUserID)
                          join c in new Group().GetEntities() on o.UserGroupID equals c.ID.ToString()
                          select c;
            return Groups.ToList();
        }


        /// <summary>
        /// 获取讨论组的用户
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public List<string> GetGroupUsers(string strGroupID)
        {
            var Users = from o in new UG().GetEntities(d => d.UserGroupID == strGroupID)
                         join c in new Yan_UserB().GetEntities() on o.UserID equals c.UserName.ToString()
                         select c.UserName ;
            return Users.ToList();
        }


        /// <summary>
        /// 退出讨论组
        /// </summary>
        /// <param name="strGroupID"></param>
        /// <param name="strUserName"></param>
        public void ExitGroup(string strGroupID, string strUserName)
        {
            new UG().Delete(d => d.UserID == strUserName && d.UserGroupID == strGroupID);
        }

        /// <summary>
        /// 是否具有发布公告的权限
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public  bool isAdmin(string strUserID)
        {

            return new User().GetEntities(" STATUS='Y' AND UserName='" + strUserID + "' ").Count() > 0;
        }

        public string GetFirGG(string strUserID)
        {
            //msgList = new vw_MsgUser().GetEntities(d => d.MsgIsRead == "0" && d.MsgUser == strUserID && d.MSGType == "0").OrderByDescending(d => d.MSGCRDate).ToList();

            List<vw_Yan_Chat_MsgUser> msgList = new List<vw_Yan_Chat_MsgUser>();
            msgList = new vw_MsgUser().GetEntitiesTop(1," MsgIsRead='0' AND  MSGType='0' AND MsgUser='" + strUserID + "' ORDER BY MSGCRDate DESC ").ToList();
            if (msgList.Count > 0)
            {
                return msgList[0].MSGTitle.ToString();
            }
            else
            {
                return "";
            }
            
        }
        /// <summary>
        /// 添加消息
        /// </summary>
        /// <param name="strFromUser"></param>
        /// <param name="strToUser"></param>
        /// <param name="Msg"></param>
        public void AddMsg(string strFromUser, string strToUser, string Msg, string isSend)
        {
            YanChat_Msg NewMsg = new YanChat_Msg();
            NewMsg.UserForm = strFromUser;
            NewMsg.UserTo = strToUser;
            NewMsg.MsgContent = Msg;
            NewMsg.IsSend = isSend;
            NewMsg.CreateDate = DateTime.Now;
            new Msg().Insert(NewMsg);
        }

        /// <summary>
        /// 根据ID获取该用户没有接收到的信息
        /// </summary>
        /// <param name="strToUserID"></param>
        /// <returns></returns>
        public List<YanChat_Msg> GetMsgByUser(string strToUserID)
        {
            List<YanChat_Msg> ListNSendMsgs = new Msg().GetEntities(d => d.UserTo == strToUserID && d.IsSend == "N").ToList();
            return ListNSendMsgs;
        }

        /// <summary>
        /// 修改用户描述
        /// </summary>
        /// <param name="strUser"></param>
        /// <param name="strUsersign"></param>
        public void UpdateUserStatus(string strUser, string strUsersign)
        {
            Yan_User UpUser = new Yan_User();
            UpUser = new User().GetEntity(d => d.UserName == strUser);
            UpUser.Usersign = strUsersign;
            new User().Update(UpUser);
        }

        /// <summary>
        /// 修改用户信息状态
        /// </summary>
        /// <param name="strUser"></param>
        /// <param name="strUsersign"></param>
        public void UpdateMsgStatus(string strMsgID)
        {
            YanChat_UserMsg UserMsg = new YanChat_UserMsg();
            UserMsg = new YanChat_UserMsgB().GetEntity(d => d.id.ToString() == strMsgID);
            UserMsg.MsgIsRead = "1";
            new YanChat_UserMsgB().Update(UserMsg);
        }

        /// <summary>
        /// 修改用户头像
        /// </summary>
        /// <param name="strUser"></param>
        /// <param name="strUsersign"></param>
        public void UpdateUserTX(string strUser, string strUserTX)
        {
            Yan_User UpUser = new Yan_User();
            UpUser = new User().GetEntity(d => d.UserName == strUser);
            UpUser.tx = strUserTX;
            new User().Update(UpUser);
        }


        /// <summary>
        /// 获取未读消息与消息总数
        /// </summary>
        /// <param name="strUser"></param>
        /// <returns></returns>
        public string GetUserXiaoXI(string strUser)
        {
            return new YanChat_UserMsgB().GetEntities(d => d.MsgUser == strUser && d.MsgIsRead == "0").Count().ToString();
        }


        //修改用户密码
        public void UpdatePad(string strUser,string strPasd)
        {
            Yan_User UpUser = new Yan_User();
            UpUser = new User().GetEntity(d => d.UserName == strUser);
            UpUser.UserPass = strPasd;
            new User().Update(UpUser);
        }
    }
}